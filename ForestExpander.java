/*
 *  File ForestExpander.java
 *
 *  Authors:
 *     Johannes Dellert  <johannes.dellert@sfs.uni-tuebingen.de>
 *     
 *  Copyright:
 *     Johannes Dellert, 2007
 *
 *  Last modified:
 *     Di 16. Okt 11:04:17 CEST 2007
 *
 *  This file is part of the TuLiPA system
 *     http://www.sfb441.uni-tuebingen.de/emmy-noether-kallmeyer/tulipa
 *
 *  TuLiPA is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  TuLiPA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/**
 * A console tool taking an XML filename as an argument and printing out the extracted derivation trees in XML format to standard out
 * @author Johannes Dellert
 */

import java.io.*;
import java.util.*;
import org.w3c.dom.*;

import javax.xml.parsers.*;

public class ForestExpander
{
    public static void main(String[] arg)
    {
        if (arg.length != 1)
        {
            System.out.println("\nNo filename specified!");
            System.exit(0);
        }
        String filename = arg[0];
        try
        {
            Document D = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(filename));
            ArrayList<Node> derivationTrees = ParseTreeHandler.extractDerivationTrees(D);
            ArrayList<XMLViewTree> viewableDerivationTrees = new ArrayList<XMLViewTree>();
            for (Node derivTree  : derivationTrees)
            {
                XMLViewTree viewTree = ViewTreeBuilder.makeViewableDerivationTree(derivTree);
                viewableDerivationTrees.add(viewTree);
            }
            derivationTrees = null;
            XMLTreeViewer.displayTrees(viewableDerivationTrees);
        }
        catch (Exception e)
        {
            System.out.println("Error while reading XML File:");
            System.out.println(e.getMessage());
            for (StackTraceElement ste : e.getStackTrace())
            {
                System.out.println(ste.toString());
            }
            System.exit(0);
        }

    }
}