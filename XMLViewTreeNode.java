/*
 *  File XMLViewTreeNode.java
 *
 *  Authors:
 *     Johannes Dellert  <johannes.dellert@sfs.uni-tuebingen.de>
 *     
 *  Copyright:
 *     Johannes Dellert, 2007
 *
 *  Last modified:
 *     Di 16. Okt 10:54:24 CEST 2007
 *
 *  This file is part of the TuLiPA system
 *     http://www.sfb441.uni-tuebingen.de/emmy-noether-kallmeyer/tulipa
 *
 *  TuLiPA is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  TuLiPA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.*;
import org.w3c.dom.*;


public class XMLViewTreeNode
{
    Node domNode;
    int id;
    int parent;
    ArrayList<Integer> children;
    ArrayList<XMLViewTreeAttribute> attrs;
    String tag;
    int x;
    int y;
    int subTreeWidth;
    String edgeTag;
    String edgeDir;
    
    
    public XMLViewTreeNode(Node domNode, int id, int parent, ArrayList<Integer> children, ArrayList<XMLViewTreeAttribute> attrs, String tag, int x, int y)
    {
        this.domNode = domNode;
        this.id = id;
        this.parent = parent;
        this.children = children;
        this.attrs = attrs;
        this.tag = tag;
        this.x = x;
        this.y = y;
        this.subTreeWidth = 1;
        this.edgeTag = "";
        this.edgeDir = "";
    }

	public Node getDomNode() {
		return domNode;
	}
    
}
