/*
 *  File XMLTreeDisplay.java
 *
 *  Authors:
 *     Johannes Dellert  <johannes.dellert@sfs.uni-tuebingen.de>
 *     
 *  Copyright:
 *     Johannes Dellert, 2007
 *
 *  Last modified:
 *     Di 16. Okt 10:51:28 CEST 2007
 *
 *  This file is part of the TuLiPA system
 *     http://www.sfb441.uni-tuebingen.de/emmy-noether-kallmeyer/tulipa
 *
 *  TuLiPA is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  TuLiPA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


import java.awt.*;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;


public class XMLTreeDisplay extends JFrame implements ListSelectionListener
{
    //serialVersionUID to avoid warning
	private static final long serialVersionUID = 1L;
	
	// added sentence parsed
	String sentence; 	

    ArrayList<XMLViewTree> trees;
     
    XMLTreePanel display;
    JScrollPane displayPane;
    
    JList parseList;
    JScrollPane parseListPane;
    
    int displayTreeID;
    
    
    JMenuBar menuBar;
    JMenu fileMenu;
    JMenuItem dumpMenuItem;
    //JMenuItem loadMenuItem;
    
    boolean displayElementaryTrees = true;
    
    public XMLTreeDisplay(ArrayList<XMLViewTree> trees) 
    {       
        this.trees = trees;
        initializeDisplay();
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
    
    public XMLTreeDisplay(String s, ArrayList<XMLViewTree> trees) 
    {       
    	this(trees);
    	sentence = s;
    }
    
    public void initializeDisplay()
    {
        
        this.parseList = new JList(trees.toArray()); 
        parseList.setMinimumSize(new Dimension(250,720));
        parseList.setVisibleRowCount(50);
        parseList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        parseList.addListSelectionListener(this);
        parseList.setVisibleRowCount(20);
        this.displayTreeID = 0;
        this.parseListPane = new JScrollPane(parseList);
        parseListPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        parseListPane.setBounds(0,0,250,720);

        buildStandardDisplay(); 
        
        displayPane.setBounds(250,0,700,720);
        
        this.getContentPane().setLayout(null);
        this.getContentPane().add(parseListPane);
        this.getContentPane().add(displayPane);

        repaint();
    }
    
    public void valueChanged(ListSelectionEvent e)
    {   
        if (e.getSource() == parseList)
        {
            int displayID = parseList.getSelectedIndex();
            displayTreeID = displayID;
            updateStandardDisplay();
        }
        repaint();
    }
    
    public void buildStandardDisplay()
    {
        if (trees.size() > 0)
        {
            //show derivation tree
            display = new XMLTreePanel();
            display.t = trees.get(displayTreeID);
            displayPane = new JScrollPane(display);
        }
    }
    
    public void updateStandardDisplay()
    {
        if (trees.size() > 0)
        {
            //show derivation tree
            display.t = trees.get(displayTreeID);
        }
    }
    
}
