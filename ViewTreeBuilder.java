/*
 *  File ViewTreeBuilder.java
 *
 *  Authors:
 *     Johannes Dellert  <johannes.dellert@sfs.uni-tuebingen.de>
 *     
 *  Copyright:
 *     Johannes Dellert, 2007
 *
 *  Last modified:
 *     Di 16. Okt 10:50:52 CEST 2007
 *
 *  This file is part of the TuLiPA system
 *     http://www.sfb441.uni-tuebingen.de/emmy-noether-kallmeyer/tulipa
 *
 *  TuLiPA is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  TuLiPA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


import java.util.*;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.*;



public class ViewTreeBuilder
{
    public static XMLViewTree makeViewableDerivationTree(Node derivTree) throws Exception
    {
        Document D = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        Node initialTree = D.importNode(derivTree.getLastChild().getFirstChild(), true);
        D.appendChild(initialTree);
        //rebuild feature structures (append them as attributes to tree nodes above)
        NodeList nargNodes = D.getElementsByTagName("narg");
        while (nargNodes.getLength() > 0)
        {
            Node nargNode = nargNodes.item(0);
            ArrayList<Node> atts = extractFeatures(nargNode, D);
            for (Node att : atts)
            {
                nargNode.getParentNode().getAttributes().setNamedItem(att);
            }
            nargNode.getParentNode().removeChild(nargNode);
        }
        //rebuild tree nodes
        NodeList treeNodes = D.getElementsByTagName("tree");
        ArrayList<Node> tNodes = new ArrayList<Node>();            
        for (int l = 0; l < treeNodes.getLength(); l++)
        {
            tNodes.add(treeNodes.item(l));    
        }
        for (int i = 0; i < tNodes.size(); i++)
        {
            Node treeNode = tNodes.get(i); 
            if (treeNode.getParentNode().getParentNode() != null)
            {
                String opType = treeNode.getParentNode().getNodeName();
                String opNode = treeNode.getParentNode().getAttributes().getNamedItem("node").getNodeValue();
                Node op = treeNode.getOwnerDocument().createAttribute("op");
                op.setNodeValue(opType); 
                treeNode.getAttributes().setNamedItem(op);
                Node opnode = treeNode.getOwnerDocument().createAttribute("op-node");
                opnode.setNodeValue(opNode); 
                treeNode.getAttributes().setNamedItem(opnode);
                Node grandfather = treeNode.getParentNode().getParentNode();
                grandfather.removeChild(treeNode.getParentNode());
                grandfather.appendChild(treeNode);
            }

        }
        //draw anchor nodes into trees
        NodeList anchorNodes = D.getElementsByTagName("anchor");
        while (anchorNodes.getLength() > 0)
        {
            Node anchorNode = anchorNodes.item(0);
            String value = anchorNode.getAttributes().getNamedItem("lex").getNodeValue();
            Node attr = D.createAttribute("anchor");
            attr.setNodeValue(value); 
            Node parent = anchorNode.getParentNode();
            parent.getAttributes().setNamedItem(attr);
            parent.removeChild(anchorNode);
        }
        //draw lexical nodes into trees
        NodeList lexicalNodes = D.getElementsByTagName("lexical");
        while (lexicalNodes.getLength() > 0)
        {
            Node lexicalNode = lexicalNodes.item(0);
            String value = lexicalNode.getAttributes().getNamedItem("lex").getNodeValue();
            Node attr = D.createAttribute("lexical");
            attr.setNodeValue(value); 
            Node parent = lexicalNode.getParentNode();
            parent.getAttributes().setNamedItem(attr);
            parent.removeChild(lexicalNode);
        }
        XMLViewTree viewTree = new XMLViewTree(initialTree); 
        viewTree.description = initialTree.getAttributes().getNamedItem("id").getNodeValue();
        for (XMLViewTreeNode n : viewTree.treeNodes)
        {
            if (n.tag.equals("tree"))
            {
                for (int i = 0; i < n.attrs.size(); i++)
                {
                    XMLViewTreeAttribute a = n.attrs.get(i);
                    if (a.name.equals("id"))
                    {
                        if (n.tag.equals("tree"))
                        {
                            n.tag = a.value;
                        }
                        else
                        {
                            n.tag = a.value + ": " + n.tag;
                        }
                        n.attrs.remove(a);
                        i--;
                    }
                    else if (a.name.equals("op-node"))
                    {
                        n.edgeTag = a.value;
                        n.attrs.remove(a);
                        i--;
                    }
                    else if (a.name.equals("anchor"))
                    {
                        if (n.tag.equals("tree"))
                        {
                            n.tag = a.value;
                        }
                        else
                        {
                            n.tag = n.tag + ": " + a.value;
                        }
                        n.attrs.remove(a);
                        i--;
                    }
                    else if (a.name.equals("op"))
                    {
                        if (a.value.equals("subst"))
                        {
                            n.edgeDir = "down";
                        }
                        else
                        {
                            n.edgeDir = "up";
                        }
                        n.attrs.remove(a);
                        i--;
                    }
                }
                if (n.attrs.size() > 0)
                {
                    viewTree.collapsedAttributes.add(n.id);
                }
            }
        }
        viewTree.createNodeLayers();
        viewTree.treeNodesDistance = 250;
        viewTree.calculateCoordinates();
        return viewTree;
    }
    
    public static ArrayList<Node> extractFeatures(Node n, Document D)
    {
        ArrayList<Node> attrs = new ArrayList<Node>();
        String type = n.getAttributes().getNamedItem("type").getNodeValue();
        NodeList features = n.getChildNodes();
        for (int i = 0; i < n.getChildNodes().getLength(); i++)
        {
            if (n.getChildNodes().item(i) instanceof Element)
            {
                features = n.getChildNodes().item(i).getChildNodes();
            }
        }
        for (int i = 0; i < features.getLength(); i++)
        {
            Node feature = features.item(i);
            if (feature instanceof Element)
            {
                String name = feature.getAttributes().getNamedItem("name").getNodeValue();
                String value = extractFeatureValue(feature);
                Node attr = D.createAttribute(type + ":" + name);
                attr.setNodeValue(value);          
                attrs.add(attr);
            }
        }
        return attrs;
    }
    
    private static String extractFeatureValue(Node n)
    {
        Node id = n.getAttributes().getNamedItem("id");
        if (id != null) return id.getNodeValue();
        Node contentChild = n;
        for (int i = 0; i < n.getChildNodes().getLength(); i++)
        {
            if (n.getChildNodes().item(i) instanceof Element)
            {
                contentChild = n.getChildNodes().item(i);
            }
        }      
        if (contentChild.getNodeName().equals("sym"))
        {
            if (contentChild.getAttributes().getNamedItem("value") != null)
            {
                return contentChild.getAttributes().getNamedItem("value").getNodeValue();
            }
            else
            {
                return contentChild.getAttributes().getNamedItem("varname").getNodeValue();
            }
        }
        else
        {
            return contentChild.getNodeName();
        }
    }
}
