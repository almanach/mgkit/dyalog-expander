/*
 *  File ParseTreeHandler.java
 *
 *  Authors:
 *     Johannes Dellert  <johannes.dellert@sfs.uni-tuebingen.de>
 *     
 *  Copyright:
 *     Johannes Dellert, 2007
 *
 *  Last modified:
 *     Di 16. Okt 11:04:50 CEST 2007
 *
 *  This file is part of the TuLiPA system
 *     http://www.sfb441.uni-tuebingen.de/emmy-noether-kallmeyer/tulipa
 *
 *  TuLiPA is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  TuLiPA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * A class to extract the parse trees from parse forests;
 * Typically, only the static method extractDerivationTrees() will be used
 * @author Johannes Dellert
 */

import org.w3c.dom.*;

import javax.xml.parsers.*;
import java.util.*;

public class ParseTreeHandler
{
    private ArrayList<ArrayList<Node>> parseHistories;
    private HashMap<Integer,Node> operations;
    private ArrayList<Node> xmlParseTrees;
    
    /**
     * Constructor creating an empty ParseTreeHandler
     */
    public ParseTreeHandler()
    {
        setParseHistories(new ArrayList<ArrayList<Node>>());
        setOperations(new HashMap<Integer,Node>());
        setXmlParseTrees(new ArrayList<Node>());
    }
    
    void setParseHistories(ArrayList<ArrayList<Node>> parseHistories)
    {
        this.parseHistories = parseHistories;
    }

    ArrayList<ArrayList<Node>> getParseHistories()
    {
        return parseHistories;
    }

    void setOperations(HashMap<Integer,Node> operations)
    {
        this.operations = operations;
    }

    HashMap<Integer,Node> getOperations()
    {
        return operations;
    }

    void setXmlParseTrees(ArrayList<Node> xmlParseTrees)
    {
        this.xmlParseTrees = xmlParseTrees;
    }

    ArrayList<Node> getXmlParseTrees()
    {
        return xmlParseTrees;
    }
    
    /**
     * Processes a parse forest XML Document and returns a new XML Document with the extracted derivation trees
     * @param doc - the parse forest DOM Document to process
     * @return a new DOM Document representing the extracted derivation trees
     */
    public static Document extractDerivationTreesAsDocument(Document doc)
    {
        //make a PTH out of the document by extracting all possible histories
        ParseTreeHandler pth = extractParseHistories(doc);
        Document D = null;
        try
        {
            //build the output document with "forest" as document element tag
            D = DocumentBuilderFactory.newInstance().newDocumentBuilder().getDOMImplementation().createDocument(null, "forest", null);
            //convert all the histories to derivation trees
            for (ArrayList<Node> history : pth.getParseHistories())
            {
                pth.getXmlParseTrees().add(extractDerivationTree(history, D));
            }
            //append the extracted derivation trees as children to the output document
            for (Node root : pth.getXmlParseTrees())
            {
                D.getDocumentElement().appendChild(root);
            }

        }
        catch (Exception e)
        {
            System.out.println("Error while building parse tree XML");
            System.out.println(e.getMessage());
            System.exit(0);
        }
        return D;
    }
    
    /**
     * Processes a parse forest XML Document and returns a list of DOM nodes heading the extracted derivation trees
     * @param doc - the parse forest DOM Document to process
     * @return a list of DOM nodes representing the extracted derivation trees
     */
    public static ArrayList<Node> extractDerivationTrees(Document doc)
    {
        //make a PTH out of the document by extracting all possible histories
        ParseTreeHandler pth = extractParseHistories(doc);
        Document D = null;
        try
        {
            //build the output document with "forest" as document element tag
            D = DocumentBuilderFactory.newInstance().newDocumentBuilder().getDOMImplementation().createDocument(null, "forest", null);
            //convert all the histories to derivation trees
            for (ArrayList<Node> history : pth.getParseHistories())
            {
                pth.getXmlParseTrees().add(extractDerivationTree(history, D));
            }
        }
        catch (Exception e)
        {
            System.out.println("Error while building parse tree XML");
            System.out.println(e.getMessage());
            System.exit(0);
        }
        return pth.getXmlParseTrees();
    }
    
    /**
     * Converts a history generated by the extractParseHistories() method into an XML derivation tree
     * @param history - an ArrayList of DOM nodes in the internal history format
     * @param D - the document to create the nodes for (i.e. the forest output document)
     * @return the "start" element whose only child is the root of the derivation tree
     */
    public static Node extractDerivationTree(ArrayList<Node> history , Document D)
    {   
        //the first node in this stack will always be the derivation we are working on
        ArrayList<Node> derivStack = new ArrayList<Node>();
        Node currentDeriv = null;
        
        //derivation always starts at an imaginary empty tree tagged "start"
        Node root = D.createElement("start");
        derivStack.add(0,root);

        //first tree will always be substituted at a node "0" in the imaginary empty tree
        String nodeID = "0";
        //traverse the history
        for (int i = 0; i < history.size(); i++)
        {
            Node currentNode = history.get(i);
            //if we meet an operation element...
            if (currentNode.getNodeName().equals("op"))
            {
                //...apply it on the current derivation by appending it to the current derivation node
                currentDeriv = derivStack.get(0);
                String type = currentNode.getAttributes().getNamedItem("type").getNodeValue();
                Node opChild = D.createElement(type);
                currentDeriv.appendChild(opChild);
                //the operation is applied on the node whose ID is the current value of nodeID
                Attr nodeAttr = D.createAttribute("node");
                nodeAttr.setValue(nodeID);
                opChild.getAttributes().setNamedItem(nodeAttr);
                //if the operation is of a type that requires another tree
                if (type.equals("subst") || type.equals("adj"))
                {
                    //extract the derivation yielding info about the tree
                    Node nextDeriv = history.get(i+1);
                    //create a new tree node for the output with the tree name as ID
                    Node treeChild = D.createElement("tree");
                    String treeID = nextDeriv.getAttributes().getNamedItem("tree").getNodeValue();
                    Attr treeAttribute = D.createAttribute("id");
                    treeAttribute.setValue(treeID);
                    treeChild.getAttributes().setNamedItem(treeAttribute);
                    //append the tree node to the current operation node
                    opChild.appendChild(treeChild);
                    //and push the derivation now visited onto the derivation stack to indicate the next operation will be performed on this tree
                    derivStack.add(0,treeChild);
                }
                //if the operation contains information on lexicalization and anchors of the current tree...
                if (type.equals("anchor") || type.equals("lexical"))
                {
                    //...append this information to the operation node
                    String lex = currentNode.getAttributes().getNamedItem("lex").getNodeValue();
                    Attr lexAttr = D.createAttribute("lex");
                    lexAttr.setValue(lex);
                    opChild.getAttributes().setNamedItem(lexAttr);
                }
            }
            //if we meet a derivation element...
            else if (currentNode.getNodeName().equals("deriv"))
            {
                //...that is only a fake derivation to mark the completion of a derivation...
                if (currentNode.getAttributes().getNamedItem("tree").getNodeValue().equals("#"))
                {
                    //...go one level higher in the derivation tree by popping the completed one from the stack
                    derivStack.remove(0);
                }
            }
            //if we meet a node element...
            else if (currentNode.getNodeName().equals("node"))
            {
                //...remember the next operation will occur at the node with its name
                nodeID = currentNode.getAttributes().getNamedItem("name").getNodeValue();
            }
            //if we meet a narg element...
            else if (currentNode.getNodeName().equals("narg"))
            {
                //...append it to the current derivation (usually father of current op element)
                Node featureNode = D.importNode(currentNode, true);
                currentDeriv = derivStack.get(0);
                currentDeriv.appendChild(featureNode);
            }
        }
        return root;
    }

    /**
     * Build a new ParseTreeHandler with histories out of a forest XML Document 
     * @param doc - the DOM Document to process
     * @return ParseTreeHandler - the new parse tree handler with histories
     */
    public static ParseTreeHandler extractParseHistories(Document doc)
    {
        ParseTreeHandler pth = new ParseTreeHandler();
        //extract all op nodes and store them in the operations hash for easy access via ID
        //this is necessary to guarantee that all oprefs can be processed from the start
        NodeList ops = doc.getElementsByTagName("op");
        for (int i = 0; i < ops.getLength(); i++)
        {
            Node currentOp = ops.item(i);
            int id = Integer.parseInt(currentOp.getAttributes().getNamedItem("id").getNodeValue());
            pth.getOperations().put(id, currentOp);
        }
        //recursively extract the parse histories of the document element
        extractParseHistories(doc.getDocumentElement(), pth);
        return pth;
    }
     
    //helper method to recursively extract the derivation histories from a forest whose virtual root node is n
    private static void extractParseHistories( Node n, ParseTreeHandler pth)
    {
        NodeList children = n.getChildNodes();
        Node child;
        for (int j = 0; j < children.getLength(); j++)
        {
            child = children.item(j);
            if (child instanceof Element)
            {
                //only interesting type of nodes here: operations (i.e. root nodes of the trees)
                if (child.getNodeName().equals("op"))
                {
                    //the parse histories will be extracted using recursive descent in forest traversal
                    pth.getParseHistories().addAll(evaluateOp(child,pth));
                }
            }
        }       
    }
    
    //helper method to evaluate op nodes during forest traversal
    private static ArrayList<ArrayList<Node>> evaluateOp(Node n, ParseTreeHandler pth)
    {
        ArrayList<ArrayList<Node>> histories = new ArrayList<ArrayList<Node>>();
        NodeList children = n.getChildNodes();
        Node child;
        ArrayList<Node> nargNodes = new ArrayList<Node>();
        for (int j = 0; j < children.getLength(); j++)
        {
            child = children.item(j);
            if (child instanceof Element)
            {
                //deriv children of op nodes are alternatives to consider
                if (child.getNodeName().equals("deriv"))
                {
                    //generate all the possible subhistories of the current alternative...
                    for (ArrayList<Node> subhistory : evaluateDeriv(child, pth))
                    {
                        //...precede them with a reference to the current operation and
                        subhistory.add(0, n);
                        //...precede them with references to the feature sets processed by this operation and
                        subhistory.addAll(0, nargNodes);
                        //...add them all to the possible histories of this operation
                        histories.add(subhistory);
                    }
                }
                //narg children of op nodes contain features for trees
                if (child.getNodeName().equals("narg"))
                {
                    nargNodes.add(child);
                }
            }
        } 
        //if no further history of the operation could be determined...
        if (histories.size() == 0)
        {
            //...state that its history consists only of itself
            ArrayList<Node> history = new ArrayList<Node>();
            history.add(n);
            histories.add(history);
        }
        return histories;
    }
    
    //  helper method to evaluate opref nodes during forest traversal
    private static ArrayList<ArrayList<Node>> evaluateOpRef(Node n, ParseTreeHandler pth)
    {
        ArrayList<ArrayList<Node>> histories = new ArrayList<ArrayList<Node>>();
        //extract the operation ID the opref is referring to ...
        int referenceIndex = Integer.parseInt(n.getAttributes().getNamedItem("ref").getNodeValue());
        //...get the corresponding operation node...
        Node opNode = pth.getOperations().get(referenceIndex);
        //...and act as if really encountering this operation node
        histories.addAll(evaluateOp(opNode,pth));
        return histories;
    }
    
    //  helper method to evaluate deriv nodes during forest traversal
    private static ArrayList<ArrayList<Node>> evaluateDeriv(Node n, ParseTreeHandler pth)
    {
        ArrayList<ArrayList<Node>> histories = new ArrayList<ArrayList<Node>>();
        NodeList children = n.getChildNodes();
        Node child;
        //seed for history expansion: the derivation itself
        ArrayList<Node> history = new ArrayList<Node>();
        history.add(n);
        histories.add(history);
        for (int j = 0; j < children.getLength(); j++)
        {
            child = children.item(j);
            if (child instanceof Element)
            {
                //all the node children (to which operations are hooked) must be traversed by each history
                if (child.getNodeName().equals("node"))
                {
                    int numberPreviousHistories = histories.size();
                    //generate all the possible subhistories of the current operation...
                    for (ArrayList<Node> subhistory : evaluateNode(child, pth))
                    {
                        //and append them to each history already built
                        for (int i = 0; i < numberPreviousHistories; i++)
                        {
                            ArrayList<Node> previousHistory = histories.get(i);
                            // modified for removing warning on the cast
                            // to be checked
                            //ArrayList<Node> pHClone = (ArrayList<Node>) previousHistory.clone(); 
                            ArrayList<Node> pHClone = new ArrayList<Node>(); 
                            for(int k = 0 ; k < previousHistory.size() ; k++){
                            	pHClone.add(previousHistory.get(k).cloneNode(true));
                            }                            	
                            pHClone.addAll(subhistory);
                            histories.add(pHClone);
                        }
                    }
                    //delete the now incomplete previous histories
                    for (int i = 0; i < numberPreviousHistories; i++)
                    {
                        histories.remove(0);
                    }
                }
            }
        }   
        //if the derivation is complete...
        for (ArrayList<Node> thisHistory : histories)
        {
            //...mark this in each history by adding a fake derivation whose tree value is "#"
            Node copy = n.cloneNode(false);
            copy.getAttributes().getNamedItem("tree").setNodeValue("#");
            thisHistory.add(copy);
        }
        return histories;
    }
    
    //helper method to evaluate deriv node nodes during forest traversal
    private static ArrayList<ArrayList<Node>> evaluateNode(Node n, ParseTreeHandler pth)
    {
        ArrayList<ArrayList<Node>> histories = new ArrayList<ArrayList<Node>>();
        NodeList children = n.getChildNodes();
        Node child;
        for (int j = 0; j < children.getLength(); j++)
        {
            child = children.item(j);
            if (child instanceof Element)
            {
                //simply evaluate op and opref children and collect their possible histories
                if (child.getNodeName().equals("op"))
                {
                    for (ArrayList<Node> subhistory : evaluateOp(child, pth))
                    {
                        //insert the node info into the history
                        subhistory.add(0,n);
                        histories.add(subhistory);
                    }
                }
                if (child.getNodeName().equals("opref"))
                {
                    for (ArrayList<Node> subhistory : evaluateOpRef(child, pth))
                    {
                        //insert the node info into the history
                        subhistory.add(0,n);
                        histories.add(subhistory);
                    }
                }
            }
        }   
        return histories;
    }
    
    /**
     * Generates a list of the histories in internal representation format
     */
    public String toString()
    {
        String output = "Histories: \n";
        for (ArrayList<Node> history : getParseHistories())
        {
            output += historyToString(history);
        }
        return output;
    }
    
    /**
     * Represents a history in internal format as a string of node IDs
     * @param history - a history in internal representation format
     * @return a string representing this history
     */
    public static String historyToString(ArrayList<Node> history)
    {
        String output = "[";
        for (Node n : history)
        {
            if (n.getNodeName().equals("op"))
            {
                output += n.getAttributes().getNamedItem("id").getNodeValue() + " ";
            }
            else
            {
                output += n.getAttributes().item(0).getNodeValue() + " ";
            }
        }
        output += "]\n";
        return output;
    }
}
