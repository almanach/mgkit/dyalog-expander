/*
 *  File XMLTreePanel.java
 *
 *  Authors:
 *     Johannes Dellert  <johannes.dellert@sfs.uni-tuebingen.de>
 *     
 *  Copyright:
 *     Johannes Dellert, 2007
 *
 *  Last modified:
 *     Di 16. Okt 10:51:59 CEST 2007
 *
 *  This file is part of the TuLiPA system
 *     http://www.sfb441.uni-tuebingen.de/emmy-noether-kallmeyer/tulipa
 *
 *  TuLiPA is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  TuLiPA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.awt.*;

import javax.swing.JPanel;

public class XMLTreePanel extends JPanel implements MouseListener
{
    //serialVersionUID to avoid warning
	private static final long serialVersionUID = 1L;
	public XMLViewTree t;
    private HashMap<String,Integer> eventGrid;
    
    public XMLTreePanel()
    {
        eventGrid = new HashMap<String,Integer>();
        this.addMouseListener(this);
    }
    
    public void paint(Graphics canvas)
    {
        //clear canvas
        Dimension newD = new Dimension(t.totalTreeWidth, t.totalTreeHeight);
        this.setSize(newD);
        this.setMinimumSize(newD);
        this.setMaximumSize(newD);
        this.setPreferredSize(newD);
        canvas.setColor(Color.WHITE);
        canvas.fillRect(0,0,2000,2000);
        canvas.fillRect(0,0,this.getSize().width,this.getSize().height);
        //print information on current tree
        for(int i = 0; i < t.nodeLevels.size(); i++)
        {
            ArrayList<Integer> nodes = t.nodeLevels.get(i);
            for (int j = 0; j < nodes.size(); j++)
            {
                //print box around node tag
                int x = t.treeNodes.get(nodes.get(j)).x - t.treeNodes.get(nodes.get(j)).tag.length() * 4 ;
                int y = t.treeNodes.get(nodes.get(j)).y - 10;
                int width = t.treeNodes.get(nodes.get(j)).tag.length() * 8;
                canvas.setColor(Color.WHITE);
                if (t.collapsedNodes.contains(nodes.get(j)))
                {
                    if (t.collapsedAttributes.contains(nodes.get(j)))
                    {
                        canvas.setColor(Color.GREEN);
                    }
                    else
                    {
                        canvas.setColor(Color.YELLOW);
                    }
                }
                else
                {
                    if (t.collapsedAttributes.contains(nodes.get(j)))
                    {
                        canvas.setColor(Color.CYAN);
                    }
                }
                canvas.fillRect(x,y,width,12);
                canvas.setColor(Color.BLACK);
                canvas.drawRect(x, y, width, 12);
                markObjectArea(nodes.get(j),x,y,width,12);

                //print tag name of node
                x = t.treeNodes.get(nodes.get(j)).x - t.treeNodes.get(nodes.get(j)).tag.length() * 3;
                y = t.treeNodes.get(nodes.get(j)).y;
                String tag = t.treeNodes.get(nodes.get(j)).tag;
                //newText.setAttribute("font-size","10");
                canvas.drawString(tag, x, y);
                
                //print attributes of node
                if (!t.collapsedAttributes.contains(nodes.get(j)))
                {
                    int attrYPos = t.treeNodes.get(nodes.get(j)).y;
                    int leftBorder = t.treeNodes.get(nodes.get(j)).x;
                    // reversed traversal of the features for distinguishing top and bot
                    ArrayList<XMLViewTreeAttribute> feats = t.treeNodes.get(nodes.get(j)).attrs; 
                    
                    for (int k = feats.size() - 1 ; k >= 0 ; k--)
                    {
                    	XMLViewTreeAttribute attr = feats.get(k);
                        int leftFringe = t.treeNodes.get(nodes.get(j)).x - (attr.name.length() + attr.value.length()) * 4;
                        if (leftFringe < leftBorder)
                        {
                            leftBorder = leftFringe;
                        }
                    }
                    for (int k = feats.size() - 1 ; k >= 0 ; k--)
                    {
                    	XMLViewTreeAttribute attr = feats.get(k);
                        attrYPos += 15;
                        if (attr.value.startsWith("#"))
                        {
                            canvas.setColor(Color.RED);
                            String attrString = attr.name + ": " + attr.value.substring(1);
                            canvas.drawString(attrString,leftBorder,attrYPos);
                        }
                        else
                        {
                            String attrString = attr.name + ": " + attr.value;
                            canvas.drawString(attrString,leftBorder,attrYPos);
                        }
                        canvas.setColor(Color.BLACK);
                    }  
                    canvas.drawRect(leftBorder, t.treeNodes.get(nodes.get(j)).y + 2, (t.treeNodes.get(nodes.get(j)).x - leftBorder) * 2, 15 * t.treeNodes.get(nodes.get(j)).attrs.size());
                }
            }
        }
        //create lines and their tags
        canvas.setColor(Color.BLACK);
        for (int i = 1; i < t.nodeLevels.size(); i++)
        {
            ArrayList<Integer> nodes = t.nodeLevels.get(i);
            for (int j = 0; j < nodes.size(); j++)
            {
                int x1 = t.treeNodes.get(t.treeNodes.get(nodes.get(j)).parent).x;
                int y1 = t.treeNodes.get(nodes.get(j)).y - t.treeLevelHeight + 2;
                int x2 = t.treeNodes.get(nodes.get(j)).x;
                int y2 = t.treeNodes.get(nodes.get(j)).y - 10;
                canvas.drawLine(x1, y1, x2, y2);
                canvas.drawString(t.treeNodes.get(nodes.get(j)).edgeTag, (x1 + x2) / 2, (y1 + y2) / 2);
                String edgeDir = t.treeNodes.get(nodes.get(j)).edgeDir;
                if (edgeDir.equals("up"))
                {
                    double slope = ((y1-y2 + 0.1) / (x1 - x2 + 0.1));
                    double lowerSlope = (slope -1)/(1+slope);
                    double higherSlope = -(1/lowerSlope);
                    Polygon arrowhead = new Polygon();
                    arrowhead.addPoint(x1, y1);
                    if((x2  <= x1 && slope < -1 )||(x2  >= x1 && slope > -1 )) arrowhead.addPoint(x1 + (int)(10/Math.sqrt(1+lowerSlope*lowerSlope)),y1 +(int)((10/Math.sqrt(1+lowerSlope*lowerSlope))*lowerSlope));
                    if((x2  <= x1 && slope > -1 )||(x2  > x1 && slope < -1 )) arrowhead.addPoint(x1 - (int)(10/Math.sqrt(1+lowerSlope*lowerSlope)),y1 -(int)((10/Math.sqrt(1+lowerSlope*lowerSlope))*lowerSlope));
                    if((x2  <= x1 && slope > 1 )||(x2  > x1 && slope < 1 )) arrowhead.addPoint(x1 + (int)(10/Math.sqrt(1+higherSlope*higherSlope)),y1 +(int)((10/Math.sqrt(1+higherSlope*higherSlope))*higherSlope));
                    if((x2  <= x1 && slope < 1 )||(x2  >= x1 && slope > 1 )) arrowhead.addPoint(x1 - (int)(10/Math.sqrt(1+higherSlope*higherSlope)),y1 -(int)((10/Math.sqrt(1+higherSlope*higherSlope))*higherSlope));
                    canvas.fillPolygon(arrowhead);
                }
                else if (edgeDir.equals("down"))
                {
                    double slope = ((y2-y1 - 0.1) / (x2 - x1 + 0.1));
                    double lowerSlope = (slope -1)/(1+slope);
                    double higherSlope = -(1/lowerSlope);
                    Polygon arrowhead = new Polygon();
                    arrowhead.addPoint(x2, y2);
                    if((x2  < x1 && slope < -1 )||(x2  >= x1 && slope > -1 )) arrowhead.addPoint(x2 - (int)(10/Math.sqrt(1+lowerSlope*lowerSlope)),y2 -(int)((10/Math.sqrt(1+lowerSlope*lowerSlope))*lowerSlope));
                    if((x2  < x1 && slope > -1 )||(x2  > x1 && slope < -1 )) arrowhead.addPoint(x2 + (int)(10/Math.sqrt(1+lowerSlope*lowerSlope)),y2 +(int)((10/Math.sqrt(1+lowerSlope*lowerSlope))*lowerSlope));
                    if((x2  < x1 && slope > 1 )||(x2  > x1 && slope < 1 )) arrowhead.addPoint(x2 - (int)(10/Math.sqrt(1+higherSlope*higherSlope)),y2 -(int)((10/Math.sqrt(1+higherSlope*higherSlope))*higherSlope));
                    if((x2  < x1 && slope < 1 )||(x2  >= x1 && slope > 1 )) arrowhead.addPoint(x2 + (int)(10/Math.sqrt(1+higherSlope*higherSlope)),y2 +(int)((10/Math.sqrt(1+higherSlope*higherSlope))*higherSlope));
                    canvas.fillPolygon(arrowhead);
                }
            }
        }
    }
    
    public void mousePressed(MouseEvent e) {}      
    public void mouseReleased(MouseEvent e) {}  
    public void mouseEntered(MouseEvent e) {}  
    public void mouseExited(MouseEvent e) {}
    
    public void mouseClicked(MouseEvent e)
    {
        String event = "c" + e.getX() + "/" + e.getY();
        if (eventGrid.get(event) != null)
        {
            if (e.getButton() == MouseEvent.BUTTON1)
            {
                t.collapseNode(eventGrid.get(event));
            }
            else
            {
                t.collapseAttributes(eventGrid.get(event));
            }
            repaint();
        }
    }
    
    private void markObjectArea(int id, int x, int y, int width, int height)
    {
        for (int i = x; i < x + width; i++)
        {
            for (int j = y; j < y + height; j++)
            {
                eventGrid.put("c" + i + "/" + j, id);
            }
        }
    }
}
