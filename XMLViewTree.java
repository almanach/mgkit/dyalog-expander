/*
 *  File XMLViewTree.java
 *
 *  Authors:
 *     Johannes Dellert  <johannes.dellert@sfs.uni-tuebingen.de>
 *     
 *  Copyright:
 *     Johannes Dellert, 2007
 *
 *  Last modified:
 *     Di 16. Okt 10:53:00 CEST 2007
 *
 *  This file is part of the TuLiPA system
 *     http://www.sfb441.uni-tuebingen.de/emmy-noether-kallmeyer/tulipa
 *
 *  TuLiPA is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  TuLiPA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.*;
import org.w3c.dom.*;


public class XMLViewTree
{
    //external information
    String description = "";
    
    //internal information
    ArrayList<ArrayList<Integer>> nodeLevels;
    ArrayList<XMLViewTreeNode> treeNodes;
    
    HashSet<Integer> collapsedNodes;
    HashSet<Integer> collapsedAttributes;
    int id;
    
    int treeNodesDistance = 200;
    int treeLevelHeight = 100;
    int totalTreeWidth;
    int totalTreeHeight;
    
    public XMLViewTree()
    {
        totalTreeWidth = 0;
        totalTreeHeight = 0;
        nodeLevels = new ArrayList<ArrayList<Integer>>();
        treeNodes = new ArrayList<XMLViewTreeNode>();
        collapsedNodes = new HashSet<Integer>();
        collapsedAttributes = new HashSet<Integer>();
    }
    
    public XMLViewTree(Node xmlNode)
    {
        createTreeStructure(xmlNode);
        createNodeLayers();
        calculateCoordinates();   
    }
    
    public XMLViewTree(Document xmlDocument)
    {
        createTreeStructure(xmlDocument.getDocumentElement());
        createNodeLayers();
        calculateCoordinates();
    }
    
    private void createTreeStructure(Node xmlNode)
    {
        id = 0;
        nodeLevels = new ArrayList<ArrayList<Integer>>();
        treeNodes = new ArrayList<XMLViewTreeNode>();
        collapsedNodes = new HashSet<Integer>();
        collapsedAttributes = new HashSet<Integer>();
        ArrayList<XMLViewTreeAttribute> attrs = new ArrayList<XMLViewTreeAttribute>();
        if (xmlNode.getAttributes() != null)
        {
            for (int j = 0; j < xmlNode.getAttributes().getLength(); j++)
            {
                XMLViewTreeAttribute attr = new XMLViewTreeAttribute();
                attr.name = xmlNode.getAttributes().item(j).getNodeName();
                attr.value = xmlNode.getAttributes().item(j).getNodeValue();
                attrs.add(attr);
            }
        }
        XMLViewTreeNode root = new XMLViewTreeNode(xmlNode,id++,-1,new ArrayList<Integer>(),attrs,retrieveTag(xmlNode), 100, 50);
        treeNodes.add(root);
        createSubtreeStructure(xmlNode,root); 
    }
    
    private void createSubtreeStructure(Node xmlNode, XMLViewTreeNode node)
    {
        for (int i = 0; i < xmlNode.getChildNodes().getLength(); i++)
        {
            Node currentChild = xmlNode.getChildNodes().item(i);
            if (contentNode(currentChild))
            {
                ArrayList<XMLViewTreeAttribute> attrs = new ArrayList<XMLViewTreeAttribute>();
                if (currentChild.getAttributes() != null)
                {
                    for (int j = 0; j < currentChild.getAttributes().getLength(); j++)
                    {
                        XMLViewTreeAttribute attr = new XMLViewTreeAttribute();
                        attr.name = currentChild.getAttributes().item(j).getNodeName();
                        attr.value = currentChild.getAttributes().item(j).getNodeValue();
                        attrs.add(attr);
                    }
                }
                XMLViewTreeNode childNode = new XMLViewTreeNode(currentChild,id++,node.id,new ArrayList<Integer>(),attrs,retrieveTag(currentChild),node.x, node.y + treeLevelHeight);
                node.x += treeNodesDistance;
                treeNodes.add(childNode);
                node.children.add(id - 1);
                createSubtreeStructure(currentChild,childNode);
            }
        }
    }
    
    public void createNodeLayers()
    {
      int level = 0;
      XMLViewTreeNode root = treeNodes.get(0);
      nodeLevels = new ArrayList<ArrayList<Integer>>();
      ArrayList<Integer> rootLevel = new ArrayList<Integer>();
      rootLevel.add(0);
      nodeLevels.add(level,rootLevel);
      level++;
      ArrayList<Integer> children = root.children;
      while(true)
      {
        ArrayList<Integer> grandchildren = new ArrayList<Integer>();
        for (int i = 0; i < children.size(); i++)
        {
            if (!collapsedNodes.contains(children.get(i)))
            {
                for (int j = 0; j < treeNodes.get(children.get(i)).children.size(); j++)
                {
                    grandchildren.add(treeNodes.get(children.get(i)).children.get(j));
                }
            }
        }
        nodeLevels.add(level,children);
        children = grandchildren;
        level++;
        if (grandchildren.size() == 0)
        {
            break;
        }
      }
    }
    
    public void calculateCoordinates()
    {
        createNodeLayers();
        totalTreeWidth = 0;
        totalTreeHeight = 0;
        //calculate (maximum) subtree width for each node bottom-up
        for(int i = nodeLevels.size() - 1; i >= 0; i--)
        {
            ArrayList<Integer> nodes = nodeLevels.get(i); 
            for (int j = 0; j < nodes.size(); j++)
            {
                XMLViewTreeNode node = treeNodes.get(nodes.get(j));
                if (!collapsedNodes.contains(nodes.get(j)) && node.children.size() > 0)
                {
                    node.subTreeWidth = collectWidths(node.children);       
                }
                else
                {
                    node.subTreeWidth = 1;
                }
            }
        }
        treeNodes.get(0).x = treeNodes.get(0).subTreeWidth * treeNodesDistance/2;
        //no edges may cross, no nodes overlap
        for(int i = 0; i < nodeLevels.size(); i++)
        {
            ArrayList<Integer> nodes = nodeLevels.get(i);  
            int xOffset = 100;
            int parent = -1;
            for (int j = 0; j < nodes.size(); j++)
            {
                int subtreeWidth = treeNodes.get(nodes.get(j)).subTreeWidth * treeNodesDistance;
                xOffset += subtreeWidth;
                if (i > 0 && treeNodes.get(nodes.get(j)).parent != parent)
                {
                    parent = treeNodes.get(nodes.get(j)).parent;
                    xOffset = (int)(treeNodes.get(parent).x +  treeNodes.get(parent).subTreeWidth * ((double)(treeNodes.get(nodes.get(j)).subTreeWidth)/treeNodes.get(parent).subTreeWidth - 0.5) * treeNodesDistance);
                }
                if (i > 0)
                {
                    treeNodes.get(nodes.get(j)).x =  xOffset - subtreeWidth/2;
                }
            }
            if (nodes.size() > 0 && treeNodes.get(nodes.get(nodes.size() - 1)).x + treeNodesDistance > totalTreeWidth)
            {
                totalTreeWidth = treeNodes.get(nodes.get(nodes.size() - 1)).x + treeNodesDistance;
            }
        }
        //make vertical space for attributes
        for (int i = 1; i < treeNodes.size(); i++)
        {
            int attHeight = 0;
            if (!collapsedAttributes.contains(treeNodes.get(i).parent))
            {
                attHeight = treeNodes.get((treeNodes.get(i)).parent).attrs.size() * 15;
            }
            treeNodes.get(i).y =  treeNodes.get((treeNodes.get(i)).parent).y + treeLevelHeight + attHeight;
            if (treeNodes.get(i).y + 100 + attHeight > totalTreeHeight)
            {
                totalTreeHeight = treeNodes.get(i).y + 100 + attHeight;
            }
        }
    }
    
    public void collapseNode(int i)
    {
        if (collapsedNodes.contains(i))
        {
            collapsedNodes.remove(i);
        }
        else
        {
            collapsedNodes.add(i);
        }
        createNodeLayers();
        calculateCoordinates();
    }
    
    public void collapseAttributes(int i)
    {
        if (collapsedAttributes.contains(i))
        {
            collapsedAttributes.remove(i);
        }
        else
        {
            collapsedAttributes.add(i);
        }
        createNodeLayers();
        calculateCoordinates();
    }
    
    public void collapseAllAttributes()
    {
        for (int i = 0; i < treeNodes.size(); i++)
        {
            if (treeNodes.get(i).attrs.size() > 0)
            {
                collapsedAttributes.add(i);
            }
        }
        createNodeLayers();
        calculateCoordinates();
    }
    
    private boolean contentNode(Node xmlNode)
    {
        if (xmlNode instanceof Element) 
        {
            return true;
        }
        if (xmlNode.getNodeValue().indexOf("\n") == -1)
        {
            return true;
        }
        return false;
    }
    
    private String retrieveTag(Node xmlNode)
    {
        if (xmlNode instanceof Element)
        {
            return xmlNode.getNodeName();
        }
        else
        {
            return xmlNode.getNodeValue();
        }
    }
    
    private int collectWidths(ArrayList<Integer> children)
    {
        int sum = 0;
        for (int i = 0; i < children.size(); i++)
        {
            sum += treeNodes.get(children.get(i)).subTreeWidth;
        }
        return sum;
    }
      
    public ArrayList<XMLViewTreeNode> getTreeNodes() {
		return treeNodes;
	}

	public String toString()
    {
        return description;
    }
}
